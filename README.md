# Arc-Theme-Firefox

My version of Offical [Arc](https://github.com/horst3180/arc-theme) Firefox theme, forked from GitHub.

## Changes

* Created ``orange`` branch with modified colors (5294e2 -> f57900)

## Original instructions

### Requirements
This theme is compatible with Firefox 40+ and Firefox 38 ESR

**Note**: This theme is meant to be used in conjunction with the [Arc GTK theme](https://github.com/horst3180/Arc-theme), don't use it with other GTK themes or it will look broken.

### Installation

You will need `autoconf` and `automake` for the following.

Clone the repository

    git clone https://gitlab.com/zlamalp/arc-theme-firefox.git && cd arc-theme-firefox

Generate the .xpi files (drag and drop these into your Firefox window)

    ./autogen.sh --prefix=/usr
    make mkxpi

Alternatively the theme can be installed globally without using the .xpi files

    ./autogen.sh --prefix=/usr
    sudo make install

Other build options to append to `autogen.sh` are

    --disable-light         disable Arc Light Firefox support
    --disable-darker        disable Arc Darker Firefox support
    --disable-dark          disable Arc Dark Firefox support

Uninstall the theme with

    sudo make uninstall

#### Firefox ESR (Debian Stable users see here)
This repo includes separate Firefox ESR compatible branches. The installation process is mostly identical to the manual installation above

    git clone https://gitlab.com/zlamalp/arc-theme-firefox.git && cd arc-theme-firefox
    git checkout firefox-38-esr   # Execute this for Firefox 38 ESR
    git checkout firefox-45-esr   # Execute this for Firefox 45 ESR
    git checkout firefox-52-esr   # Execute this for Firefox 52 ESR
    ./autogen.sh --prefix=/usr
    make mkxpi
